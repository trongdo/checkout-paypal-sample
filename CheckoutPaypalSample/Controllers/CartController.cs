﻿using System;
using CheckoutPaypalSample;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PayPalCheckoutSdk.Orders;

namespace PaymentWithPaypalSample.Controllers
{
    [EnableCors("_myAllowSpecificOrigins")]
    [Route("[controller]")]
    [ApiController]
    public class CartController : Controller
    {
        [HttpPost]
        public IActionResult Checkout()
        {
            var response = PaypalService.CreateOrder().Result;
            var result = response.Result<Order>();
            
            foreach (var link in result.Links)
            {
                if (link.Rel.Equals("approve"))
                {
                    Console.WriteLine(link.Href);
                }
            }
            
            Console.WriteLine("Create Order Response Status: " + response.StatusCode);
            
            return Ok(JsonConvert.SerializeObject(result));
        }

        [HttpPost("Success")]
        public IActionResult CaptureOrder([FromBody]string orderId)
        {
            Console.WriteLine(orderId);
            var response = PaypalService.CaptureOrder(orderId).Result;
            Console.WriteLine("Capture Order Response Status: " + response.StatusCode);
            var result = response.Result<Order>();
            return Ok(JsonConvert.SerializeObject(result));
        }
    }
}