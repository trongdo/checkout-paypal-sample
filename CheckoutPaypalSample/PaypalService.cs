﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using PaymentWithPaypalSample;
using PayPalCheckoutSdk.Orders;
using PayPalHttp;

namespace CheckoutPaypalSample
{
    public class PaypalService
    {
        private static OrderRequest BuildRequestBody()
        {
            OrderRequest orderRequest = new OrderRequest()
            {
                CheckoutPaymentIntent = "CAPTURE",

                ApplicationContext = new ApplicationContext
                {
                    BrandName = "ABC Bakery",
                    LandingPage = "BILLING",
                    UserAction = "CONTINUE",
                    ShippingPreference = "SET_PROVIDED_ADDRESS"
                },
                PurchaseUnits = new List<PurchaseUnitRequest>
                {
                    new PurchaseUnitRequest{
                        ReferenceId =  "PUHF",
                        Description = "Food",
                        CustomId = "CUST-HighFashions",
                        SoftDescriptor = "HighFashions",
                        Payee = new Payee()
                        {
                            Email = "sb-57hke5157327@personal.example.com"
                        },
                        AmountWithBreakdown = new AmountWithBreakdown
                        {
                            CurrencyCode = "USD",
                            Value = "5040.00",
                            AmountBreakdown = new AmountBreakdown
                            {
                                ItemTotal = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "5000.00"
                                },
                                Shipping = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "20.00"
                                },
                                Handling = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "10.00"
                                },
                                TaxTotal = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "20.00"
                                },
                                ShippingDiscount = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "10.00"
                                }
                            }
                        },
                        Items = new List<Item>
                        {
                            new Item
                            {
                                Name = "YEAST FREE BUTTERY CINNAMON RAISIN BREAD",
                                Description = "Large",
                                Sku = "sku01",
                                UnitAmount = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "2500.00"
                                },
                                Tax = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "10.00"
                                },
                                Quantity = "1",
                                Category = "PHYSICAL_GOODS"
                            },
                            new Item
                            {
                                Name = "FRESH STRAWBERRY BREAD",
                                Description = "Small",
                                Sku = "sku02",
                                UnitAmount = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "1250.00"
                                },
                                Tax = new Money
                                {
                                    CurrencyCode = "USD",
                                    Value = "5.00"
                                },
                                Quantity = "2",
                                Category = "PHYSICAL_GOODS"
                            }
                        },
                        ShippingDetail = new ShippingDetail
                        {
                            Name = new Name
                            {
                                FullName = "John Doe"
                            },
                            AddressPortable = new AddressPortable
                            {
                                AddressLine1 = "123 Townsend St",
                                AddressLine2 = "Floor 6",
                                AdminArea2 = "San Francisco",
                                AdminArea1 = "CA",
                                PostalCode = "94107",
                                CountryCode = "US"
                            }
                        }
                    }
                }
            };
            return orderRequest;
        }
        
        public static async Task<HttpResponse> CreateOrder()
        {
            var request = new OrdersCreateRequest();
            request.RequestBody(BuildRequestBody());
            var response = await PaypalClient.Client().Execute(request);
            return response;
        }

        public static async Task<HttpResponse> CaptureOrder(string orderId)
        {
            var request = new OrdersCaptureRequest(orderId);
            request.Headers.Add("PayPal-Mock-Response", "{\"mock_application_codes\" : \"INSTRUMENT_DECLINED\"}");
            request.RequestBody(new OrderActionRequest());
            var response = await PaypalClient.Client().Execute(request);
            return response;
        }
    }
}